module Main where

import AdventEightLib
import AdventFiveLib
import AdventFourLib
import AdventNineLib
import AdventOneLib
import AdventSevenLib
import AdventSixLib
import AdventTenLib
import AdventThreeLib
import AdventTwoLib

import System.Environment

chooser :: String -> [String] -> IO ()
chooser "one" = adventOne
chooser "two" = adventTwo
chooser "three" = adventThree
chooser "four" = adventFour
chooser "five" = adventFive
chooser "six" = adventSix
chooser "seven" = adventSeven
chooser "eight" = adventEight
chooser "nine" = adventNine
chooser "ten" = adventTen

main :: IO ()
main = do
  (day:args) <- getArgs
  chooser day args
