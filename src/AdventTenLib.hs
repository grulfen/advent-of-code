module AdventTenLib
  ( adventTen
  ) where

import Data.List (nub, null, sort)
import Data.List.Split (splitOn)

diff :: [Int] -> [Int]
diff lst = zipWith (flip (-)) lst (tail lst)

numOneMultNumThree :: [Int] -> Int
numOneMultNumThree diffs = numOneDiff * numThreeDiff
  where
    numOneDiff = length $ filter (== 1) diffs
    numThreeDiff = length $ filter (== 3) diffs

joltages :: [String] -> [Int]
joltages input = [0] ++ adapters ++ [maximum adapters + 3]
  where
    adapters = sort $ map read input

numArrangements :: [Int] -> Int
numArrangements adapters = product (map (tribonacci . (+ 2) . length) groups)
  where
    groups = filter (not . null) $ splitOn [3] adapters

tribonacci :: Int -> Int
tribonacci 0 = 0
tribonacci 1 = 0
tribonacci 2 = 1
tribonacci n = tribonacci (n - 1) + tribonacci (n - 2) + tribonacci (n - 3)

adventTen :: [String] -> IO ()
adventTen args = do
  adapters <- fmap lines getContents
  print . numArrangements . diff $ joltages adapters
