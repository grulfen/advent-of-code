module AdventFourLib
  ( adventFour
  , validateFields
  , parsePasswords
  , fields
  ) where

import Data.List (intercalate)
import Data.List.Split (splitOn)
import Data.Maybe (fromJust, isJust, isNothing)
import Text.Regex

type Passport = [(String, String)]

parseByr = return . read

validateByr :: Int -> Maybe Int
validateByr year
  | 1920 <= year && year <= 2002 = Just year
  | otherwise = Nothing

parseIyr = return . read

validateIyr :: Int -> Maybe Int
validateIyr year
  | 2010 <= year && year <= 2020 = Just year
  | otherwise = Nothing

parseEyr = return . read

validateEyr :: Int -> Maybe Int
validateEyr year
  | 2020 <= year && year <= 2030 = Just year
  | otherwise = Nothing

parseHgt = matchRegex $ mkRegex "^([0-9]+)(cm|in)$"

validateHgt :: [String] -> Maybe Int
validateHgt [hgt, "cm"]
  | 150 <= height && height <= 193 = Just height
  | otherwise = Nothing
  where
    height = read hgt
validateHgt [hgt, "in"]
  | 59 <= height && height <= 76 = Just height
  | otherwise = Nothing
  where
    height = read hgt

parseHcl = matchRegex $ mkRegex "^#[0-9a-f]{6}$"

validateHcl _ = Just True

parseEcl = matchRegex $ mkRegex "^(amb|blu|brn|gry|grn|hzl|oth)$"

validateEcl _ = Just True

parsePid = matchRegex $ mkRegex "^[0-9]{9}$"

validatePid _ = Just True

validate ::
     String
  -> (String -> Maybe a)
  -> (a -> Maybe b)
  -> Passport
  -> Maybe Passport
validate field parseFunc validFunc passport =
  lookup field passport >>= parseFunc >>= validFunc >> return passport

validateFunctions :: [Passport -> Maybe Passport]
validateFunctions =
  [ validate "byr" parseByr validateByr
  , validate "iyr" parseIyr validateIyr
  , validate "eyr" parseEyr validateEyr
  , validate "hgt" parseHgt validateHgt
  , validate "hcl" parseHcl validateHcl
  , validate "ecl" parseEcl validateEcl
  , validate "pid" parsePid validatePid
  ]

validateFields :: Passport -> Bool
validateFields passport =
  isJust $ foldr (=<<) (return passport) validateFunctions

fields :: String -> Passport
fields passport = map (only2 . splitOn ":") $ words passport
  where
    only2 [key, value] = (key, value)

parsePasswords :: String -> [Passport]
parsePasswords batch = map (fields . unwords) $ splitOn [""] $ lines batch

adventFour :: [String] -> IO ()
adventFour args = do
  passwords <- getContents
  print $ length $ filter validateFields $ parsePasswords passwords
