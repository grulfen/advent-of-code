module AdventFiveLib
  ( adventFive
  , calcSeatId
  ) where

import Data.List

seatId :: (Int, Int) -> Int
seatId (row, column) = row * 8 + column

calcRow :: String -> Int
calcRow rowStr = foldr func 0 $ zip (reverse rowStr) $ map (2 ^) [0 ..]
  where
    func ('B', i) acc = i + acc
    func ('F', i) acc = acc

calcColumn :: String -> Int
calcColumn colStr = foldr func 0 $ zip (reverse colStr) $ map (2 ^) [0 ..]
  where
    func ('R', i) acc = i + acc
    func ('L', i) acc = acc

getRowAndCol :: String -> (String, String)
getRowAndCol = splitAt 7

calcSeatId :: String -> Int
calcSeatId boardingpass = seatId (row, column)
  where
    row = calcRow rowStr
    column = calcColumn columnStr
    (rowStr, columnStr) = getRowAndCol boardingpass

findMissingSeats :: [Int] -> [Int]
findMissingSeats seatIds = [(minimum seatIds) .. (maximum seatIds)] \\ seatIds

hasNeighbours :: [Int] -> Int -> Bool
hasNeighbours seatIds seat = all (`elem` seatIds) [pred seat, succ seat]

findEmptySeat :: [Int] -> Int
findEmptySeat seatIds =
  head $ filter (hasNeighbours seatIds) $ findMissingSeats seatIds

adventFive :: [String] -> IO ()
adventFive args = do
  boardingPasses <- fmap lines getContents
  print $ findEmptySeat $ map calcSeatId boardingPasses
