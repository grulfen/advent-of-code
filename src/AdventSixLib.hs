module AdventSixLib
  ( adventSix
  ) where

import Data.List (intersect)
import Data.List.Split (splitOn)

adventSix :: [String] -> IO ()
adventSix args = do
  answers <- getContents
  print $ sum $ map (length . foldr1 intersect) $ splitOn [""] $ lines answers
