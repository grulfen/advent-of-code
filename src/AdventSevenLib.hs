module AdventSevenLib
  ( adventSeven
  , makeRule
  ) where

import Data.List.Split (splitOn)
import qualified Data.Map as Map
import Data.Map ((!))
import Data.Maybe (fromJust, isNothing)
import Text.Regex

ruleRegex = mkRegex "^(.* bag)s contain (.*).$"

contentsRegex = mkRegex "([0-9]+) (.* bag)s?"

type Bag = String

type Rules = Map.Map Bag (Maybe [(Int, Bag)])

-- {{{ parsing code
oneContent :: String -> (Int, Bag)
oneContent str = (read num, bag)
  where
    [num, bag] = fromJust $ matchRegex contentsRegex str

makeContents :: String -> Maybe [(Int, Bag)]
makeContents "no other bags" = Nothing
makeContents contentStr = Just $ map oneContent $ splitOn "," contentStr

makeRule :: String -> (Bag, Maybe [(Int, Bag)])
makeRule line = (bag, makeContents contents)
  where
    [bag, contents] = fromJust $ matchRegex ruleRegex line

bagRules :: String -> Rules
bagRules bagRulesStr = Map.fromList $ map makeRule $ lines bagRulesStr

-- }}}
-- {{{ part 1
isInBag :: Rules -> Bag -> (Int, Bag) -> Bool
isInBag rules wantedBag (_, bag)
  | bag == wantedBag = True
  | otherwise = anyContainsBag rules wantedBag (rules ! bag)

anyContainsBag :: Rules -> Bag -> Maybe [(Int, Bag)] -> Bool
anyContainsBag rules _ Nothing = False
anyContainsBag rules wantedBag (Just contents) =
  any (isInBag rules wantedBag) contents

numContainedBy :: Rules -> Bag -> Int
numContainedBy rules bag =
  length $ filter (anyContainsBag rules bag) $ Map.elems rules

-- }}}
-- {{{ part 2
func :: Rules -> (Int, Bag) -> Int
func rules (num, bag) = num + num * numBagsInside rules bag

numBagsInside :: Rules -> Bag -> Int
numBagsInside rules bag
  | isNothing (rules ! bag) = 0
  | otherwise = sum $ map (func rules) $ fromJust (rules ! bag)

-- }}}
adventSeven :: [String] -> IO ()
adventSeven args = do
  rulesStr <- getContents
  print $ numBagsInside (bagRules rulesStr) (head args)
