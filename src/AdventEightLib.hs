module AdventEightLib
  ( adventEight
  ) where

import Data.List (nub)
import Data.Maybe (fromJust)
import Text.Regex

instrRegex = mkRegex "(nop|acc|jmp) \\+?(-?[0-9]+)"

type Instr = (String, Int)

type Program = [Instr]

data Cpu =
  Cpu
    { pm :: Program
    , acc :: Int
    , pc :: Int
    , history :: [Int]
    }
  deriving (Show)

parseInstr :: String -> Instr
parseInstr str = (instr, read arg)
  where
    [instr, arg] = fromJust $ matchRegex instrRegex str

initCpu :: Program -> Cpu
initCpu prg = Cpu prg 0 0 []

runInstr :: Cpu -> Instr -> Cpu
runInstr (Cpu pm acc pc history) ("nop", _) =
  Cpu pm acc (pc + 1) (nub (pc : history))
runInstr (Cpu pm acc pc history) ("acc", inc) =
  Cpu pm (acc + inc) (pc + 1) (nub (pc : history))
runInstr (Cpu pm acc pc history) ("jmp", jmp) =
  Cpu pm acc (pc + jmp) (nub (pc : history))

step :: Cpu -> Cpu
step cpu = runInstr cpu (pm cpu !! pc cpu)

willRunInstrAgain :: Cpu -> Bool
willRunInstrAgain (Cpu _pm _acc pc history) = pc `elem` history

runUntilLoop :: Cpu -> Cpu
runUntilLoop cpu = head $ filter willRunInstrAgain $ iterate step cpu

hasTerminated :: Cpu -> Bool
hasTerminated (Cpu pm acc pc history) = pc == length pm

runUntilEnd :: Cpu -> Cpu
runUntilEnd cpu = head $ filter hasTerminated $ iterate step cpu

willTerminate :: Cpu -> Bool
willTerminate cpu
  | hasTerminated cpu = True
  | willRunInstrAgain cpu = False
  | otherwise = willTerminate $ step cpu

modifyInstr :: Instr -> Instr
modifyInstr ("nop", n) = ("jmp", n)
modifyInstr ("jmp", n) = ("nop", n)
modifyInstr (instr, n) = (instr, n)

modifyProgram :: Int -> Program -> Program
modifyProgram i program = start ++ [modifyInstr instr] ++ end
  where
    start = take i program
    instr = program !! i
    end = drop (i + 1) program

modifiedPrograms :: Program -> [Program]
modifiedPrograms origPrg =
  nub $ map (`modifyProgram` origPrg) [0 .. length origPrg - 1]

adventEight :: [String] -> IO ()
adventEight args = do
  program <- fmap lines getContents
  print $
    acc $
    runUntilEnd $
    head $
    filter willTerminate $
    map initCpu $ modifiedPrograms $ map parseInstr program
