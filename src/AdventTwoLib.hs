module AdventTwoLib
  ( adventTwo
  , parsePassword
  , isValid
  , isValid2
  ) where

import Data.Char
import Data.List.Split

type Password = ((Int, Int), Char, String)

countElem :: Eq a => a -> [a] -> Int
countElem c lst = length $ filter (c ==) lst

parsePassword :: String -> Password
parsePassword line = ((read from, read to), char, password)
  where
    [from, to, [char], _, password] = splitOneOf "-: " line

isValid :: Password -> Bool
isValid ((from, to), char, password) = from <= n && n <= to
  where
    n = countElem char password

isValid2 :: Password -> Bool
isValid2 ((i, j), char, password) = countElem True charsInPositions == 1
  where
    charsInPositions = map ((char ==) . (password !!)) [i - 1, j - 1]

adventTwo :: [String] -> IO ()
adventTwo args = do
  passwords <- fmap lines getContents
  print $ length $ filter isValid2 $ map parsePassword passwords
