module AdventNineLib
  ( adventNine
  , slidingWindow
  , subListWithSum
  ) where

import AdventOneLib (nWise)
import Data.List (concat, nub)

pairWise :: [Int] -> [[Int]]
pairWise = nWise 2

valid :: ([Int], Int) -> Bool
valid (preamble, n) = elem n $ map sum $ pairWise $ nub preamble

slidingWindow :: [Int] -> Int -> [[Int]]
slidingWindow lst@(x:xs) n
  | length lst <= n = [lst]
  | otherwise = take n lst : slidingWindow xs n

windowAndNext :: [Int] -> Int -> [([Int], Int)]
windowAndNext lst n = zip (slidingWindow lst n) $ drop n lst

subListWithSum :: [Int] -> Int -> [Int]
subListWithSum lst n =
  head $ filter (sumsTo n) $ concatMap (slidingWindow lst) [2 ..]
  where
    sumsTo n lst = sum lst == n

invalidNum :: [Int] -> Int -> Int
invalidNum lst n = snd $ head $ filter (not . valid) $ windowAndNext lst n

secretSequence :: [Int] -> Int -> [Int]
secretSequence intLst n = subListWithSum intLst $ invalidNum intLst n

secretNumber :: [String] -> Int -> Int
secretNumber lst n = minimum secretSeq + maximum secretSeq
  where
    secretSeq = secretSequence (map read lst) n

adventNine :: [String] -> IO ()
adventNine args = do
  numSequence <- fmap lines getContents
  print $ secretNumber numSequence $ read $ head args
