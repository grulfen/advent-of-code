module AdventOneLib
  ( adventOne
  , adventOnePure
  , nWise
  ) where

nWise :: Int -> [Int] -> [[Int]]
nWise 0 xs = [[]]
nWise n [] = []
nWise n (x:xs) = map (x :) (nWise (n - 1) xs) ++ nWise n xs

nWithSum :: Int -> Int -> [Int] -> [Int]
nWithSum n s xs = head $ filter (\a -> sum a == s) $ nWise n xs

productOfNNumbersWithSum :: Int -> Int -> [Int] -> Int
productOfNNumbersWithSum n s xs = product $ nWithSum n s xs

parseInts :: String -> [Int]
parseInts str = map read $ lines str

adventOnePure :: Int -> Int -> String -> Int
adventOnePure n s str = productOfNNumbersWithSum n s $ parseInts str

adventOne :: [String] -> IO ()
adventOne args = do
  numberString <- getContents
  print $ adventOnePure n s numberString
  where
    [n, s] = map read args
