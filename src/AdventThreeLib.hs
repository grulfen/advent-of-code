module AdventThreeLib
  ( adventThree
  , slopeCoords
  , coordinates
  , countTrees
  ) where

import Data.List

type TobogganMap = [String]

getCoordinate :: TobogganMap -> (Int, Int) -> Char
getCoordinate tMap (x, y) = cell
  where
    row = tMap !! y
    cell = row !! mod x (length row)

slopeCoords :: [(Int, Int)]
slopeCoords = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

isTree :: TobogganMap -> (Int, Int) -> Bool
isTree tMap coord = getCoordinate tMap coord == '#'

coordinates :: (Int, Int) -> [(Int, Int)]
coordinates (dx, dy) = iterate (\(x, y) -> (x + dx, y + dy)) (0, 0)

countTrees :: TobogganMap -> (Int, Int) -> Int
countTrees tMap slope =
  length $
  filter (isTree tMap) $ takeWhile (\(_, y) -> y < maxY) (coordinates slope)
  where
    maxY = length tMap

adventThree :: [String] -> IO ()
adventThree args = do
  tobogganMap <- fmap lines getContents
  print $ product $ map (countTrees tobogganMap) slopeCoords
