module AdventFiveSpec where

import AdventFiveLib
import Test.Hspec

spec :: Spec
spec = do
  describe "adventFive" $ do
    describe "part one example" $ do
      it "boarding pass one" $ calcSeatId "BFFFBBFRRR" `shouldBe` 567
      it "boarding pass two" $ calcSeatId "FFFBBBFRRR" `shouldBe` 119
      it "boarding pass three" $ calcSeatId "BBFFBBFRLL" `shouldBe` 820
