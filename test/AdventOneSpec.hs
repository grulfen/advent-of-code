module AdventOneSpec where

import AdventOneLib
import Test.Hspec

exampleStr :: String
exampleStr = unlines $ map show [1721, 979, 366, 299, 675, 1456]

spec :: Spec
spec = do
  describe "nWise" $ do
    it "can break a list into elements of length 1" $
      nWise 1 [1, 2, 3] `shouldBe` [[1], [2], [3]]
    it "can break a list into elements of length 2" $
      nWise 2 [1, 2, 3] `shouldBe` [[1, 2], [1, 3], [2, 3]]
    it "can break a list into elements of length 3" $
      nWise 3 [1, 2, 3] `shouldBe` [[1, 2, 3]]
    it "gives an empty list if more elements are requested than in the list" $
      nWise 3 [1, 2] `shouldBe` []
  describe "adventOne" $ do
    it "part one example" $ adventOnePure 2 2020 exampleStr `shouldBe` 514579
    it "part two example" $ adventOnePure 3 2020 exampleStr `shouldBe` 241861950
