module AdventTwoSpec where

import AdventTwoLib
import Test.Hspec

examplePasswords :: [String]
examplePasswords = ["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]

spec :: Spec
spec = do
  describe "adventTwo" $ do
    context "part one" $ do
      it "part one example" $
        length (filter isValid $ map parsePassword examplePasswords) `shouldBe`
        2
      it "valid password is valid" $
        isValid (parsePassword "1-3 a: abcde") `shouldBe` True
      it "invalid password is invalid" $
        isValid (parsePassword "1-3 b: cdefg") `shouldBe` False
    context "part two" $ do
      it "part two example" $
        length (filter isValid2 $ map parsePassword examplePasswords) `shouldBe`
        1
      it "password with one matching char in given position is valid" $
        isValid2 (parsePassword "1-3 a: abcde") `shouldBe` True
      it "password with no matching chars in given possitions is invalid" $
        isValid2 (parsePassword "1-3 b: cdefg") `shouldBe` False
      it "password with two matching chars in given positions is invalid" $
        isValid2 (parsePassword "2-9 c: ccccccccc") `shouldBe` False
