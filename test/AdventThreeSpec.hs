module AdventThreeSpec where

import AdventThreeLib
import Test.Hspec

exampleMap :: [String]
exampleMap =
  [ "..##......."
  , "#...#...#.."
  , ".#....#..#."
  , "..#.#...#.#"
  , ".#...##..#."
  , "..#.##....."
  , ".#.#.#....#"
  , ".#........#"
  , "#.##...#..."
  , "#...##....#"
  , ".#..#...#.#"
  ]

spec :: Spec
spec = do
  describe "adventThree" $ do
    it "part one example" $ countTrees exampleMap (3, 1) `shouldBe` 7
    it "part two example" $
      product (map (countTrees exampleMap) slopeCoords) `shouldBe` 336
